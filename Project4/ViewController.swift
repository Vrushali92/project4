//
//  ViewController.swift
//  Project4
//
//  Created by Vrushali Kulkarni on 13/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit
import WebKit

final class ViewController: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var progressView: UIProgressView!
    var selectedWebsite: String?

    override func loadView() {

        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView

//        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Open",
//                                                            style: .plain,
//                                                            target: self,
//                                                            action: #selector(openTapped))
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureWebView()
        configureUIBarButton()
    }
}

// MARK: Configure UI
private extension ViewController {

    func configureUIBarButton() {

        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh,
                                      target: webView,
                                      action: #selector(webView.reload))

        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                    target: nil,
                                    action: nil)

        let back = UIBarButtonItem(title: "Back",
                                   style: .plain,
                                   target: webView,
                                   action: #selector(webView.goBack))

        let front = UIBarButtonItem(title: "Front",
                                    style: .plain,
                                    target: webView,
                                    action: #selector(webView.goForward))

        let progressButton = configureProgressView()
        toolbarItems = [back, progressButton, space, refresh, front]
        navigationController?.isToolbarHidden = false
    }

    func configureProgressView() -> UIBarButtonItem {

        progressView = UIProgressView(progressViewStyle: .default)
        progressView.sizeToFit()

        return UIBarButtonItem(customView: progressView)
    }
}

private extension ViewController {

    func openPage(action: UIAlertAction) {

        guard let actionTitle = action.title,
              let url = URL(string: "https://" + actionTitle) else { return }

        webView.load(URLRequest(url: url))
    }

    func configureWebView() {

        guard let selectedWebsite = selectedWebsite else { return }
        let url = URL(string: "https://" + selectedWebsite)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        webView.addObserver(self,
                            forKeyPath: #keyPath(WKWebView.estimatedProgress),
                            options: .new,
                            context: nil)
    }

    func showAlert() {
        let alertController = UIAlertController(title: "Warning",
                                                message: "This site is blocked",
                                                preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: Objective C functions
private extension ViewController {

//    @objc func openTapped() {
//
//        let alertController = UIAlertController(title: "Open",
//                                                message: nil,
//                                                preferredStyle: .actionSheet)
//        for website in websites {
//            let action = UIAlertAction(title: website,
//                                        style: .default,
//                                        handler: openPage)
//            alertController.addAction(action)
//        }
//
//        let action3 = UIAlertAction(title: "Cancel",
//                                    style: .cancel,
//                                    handler: nil)
//
//        alertController.addAction(action3)
//        alertController.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
//        present(alertController, animated: true, completion: nil)
//    }
}

// MARK: WKNavigationDelegate function
extension ViewController {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }

    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

        let url = navigationAction.request.url
        if let host = url?.host,
            let selectedWebsite = selectedWebsite {
                if host.contains(selectedWebsite) {
                    decisionHandler(.allow)
                    return
                } else {
                    showAlert()
                }
        }
        decisionHandler(.cancel)
    }
}

// MARK: NSKeyValueObserving function
extension ViewController {

    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            progressView.progress = Float(webView.estimatedProgress)
        }
    }
}

