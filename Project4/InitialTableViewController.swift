//
//  InitialTableViewController.swift
//  Project4
//
//  Created by Vrushali Kulkarni on 14/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class InitialTableViewController: UITableViewController {

    private var websites = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Websites"
        readWebsitesFromFile()
    }

    func readWebsitesFromFile() {

        if let path = Bundle.main.path(forResource: "Websites", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: path)
                websites = contents.components(separatedBy: "\n")
            } catch {
                print(error)
            }
        }
    }
}

// MARK: - Table view data source and delegate

extension InitialTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return websites.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InitialCell", for: indexPath)
        cell.textLabel?.text = websites[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let viewController = storyboard?.instantiateViewController(withIdentifier: "ViewController")
            as? ViewController {
            viewController.selectedWebsite = websites[indexPath.row]
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
